#! /bin/bash

apt-add-repository -y ppa:webupd8team/java
apt-add-repository -y ppa:jtaylor/keepass
add-apt-repository -y ppa:nathan-renniewaldock/flux
apt-add-repository -y ppa:gnome-terminator/ppa
apt-add-repository -y ppa:unit193/encryption
apt-add-repository -y ppa:webupd8team/atom 
add-apt-repository -y ppa:mmk2410/intellij-idea-community

apt-get update
apt-get -y upgrade

apt-get install -y oracle-java8-installer
apt-get install -y keepass2
apt-get install -y fluxgui
apt-get install -y terminator
apt-get install -y veracrypt
apt-get install -y atom
apt-get install -y intellij-idea-community

apt-get install -y emacs
git clone https://github.com/flyingmachine/emacs-for-clojure.git .emacs.d

mkdir .bin
wget "https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein" -P .bin
chmod a+x .bin/lein
.bin/lein

echo "PROGRAMS=~/.bin" >> .bashrc
echo "LEIN=$PROGRAMS/lein" >> .bashrc
echo "PATH=$LEIN:$PATH" >> .bashrc